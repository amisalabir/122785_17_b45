<?php
/**
 * Created by PhpStorm.
 * User: OIT
 * Date: 28/01/2017
 * Time: 12:54 AM
 */

namespace App\Model;
use PDO, PDOException;

class Database
{
    public $dbh;

    public function __construct()
    {
        try{
            $this->dbh= new PDO("mysql:host=localhost;dbname=atomic_Project_b45", "root", "");
            $this->dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            echo "Database Connection Successful !<br>";
        }
        catch(PDOException $error) {
            echo "Database Error ".$error->getMessage();
        }
    }
}