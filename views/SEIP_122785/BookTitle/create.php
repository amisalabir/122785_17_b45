<?php
    require_once ("../../../vendor/autoload.php");
use App\Message\Message;
    if(!isset($_SESSION)){
        session_start();
    }

    $msg=Message::getMessage();
  //  echo  "<div id='message'> $msg</div>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Create Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">
</head>
<body>
<div class="container">
    <div id="message" class="errorMessage"> <?php echo   $msg; ?></div>
</div>

<div class="container">



    <form action="store.php" method="post">

        <div class="studentInformation"  >

            <div class="form-group">
                <label for="name">Enter Book Name:</label>
                <input type="text" class="form-control" name="bookName" placeholder="Please Enter Book's Name Here...">
            </div>

            <div class="form-group">
                <label for="studentID">Author Name:</label>
                <input type="text" class="form-control" name="authorName" placeholder="Please Enter Author's Name ID Here...">
            </div>
            <button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>

        </div>
        </div>
    </form>

</div>
<script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>