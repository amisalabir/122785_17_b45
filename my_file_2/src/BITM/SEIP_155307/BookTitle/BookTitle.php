<?php


namespace App\BookTitle;


use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB
{

    private $id;
    private  $book_name;
    private $author_name;

    public  function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){

            $this->id=$allPostData['id'];
        }
        if(array_key_exists("bookName",$allPostData)){

            $this->book_name=$allPostData['bookName'];
        }
        if(array_key_exists("author",$allPostData)){

            $this->author_name=$allPostData['author'];
        }
    }

    public function store(){
        $arrayData=array($this->book_name,$this->author_name);

        $query='INSERT INTO book_title(book_name, author_name) VALUES (?,?)';

        $STH=$this->DBH->prepare($query);

        $result=$STH->execute($arrayData);
        if($result){
            Message::setmessage("Success ! Data has been inserted");
                   }
        else {
            Message::setmessage("Failed ! Data has not been inserted");
        }

        Utility::redirect('create.php');
    }


}